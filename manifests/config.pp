#
# Generate a Tivoli configuration file.

define tivoli_client::config(
    $servername = "bars4",
    $nodename = "",
    $tcpport = "1400",
    $tcpserveraddress = "tavern",
    $passwordaccess = "generate",
    $memoryef = "no",
    $encrypt = "no")
{
    file {
        "/etc/tivoli/dsm.opt":
            content => template("tivoli_client/dsm.opt.erb"),
            notify  => Service["dsmc"];
        "/etc/tivoli/dsm.sys":
            content => template("tivoli_client/dsm.sys.erb"),
            notify  => Service["dsmc"];
    }
}