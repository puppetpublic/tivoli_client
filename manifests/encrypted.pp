# enable encryption for TSM backups
class tivoli_client::encrypted inherits tivoli_client {
    Tivoli_client::Config[$::fqdn_lc] {
        encrypt => "yes"
    }
}
