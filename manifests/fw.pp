# TSM client behind old firewall with a default TSM server
class tivoli_client::fw inherits tivoli_client {
  Tivoli_client::Config[$::fqdn_lc] {
    tcpserveraddress => 'backdoor',
  }
}
