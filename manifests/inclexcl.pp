# Generate a Tivoli inclexcl file.
#
# EXAMPLE:
#  class blah inherits tivoli_client {
#      Tivoli_client::Inclexcl[$::fqdn_lc] {
#          xfs => [ '/dir', '/nuther/dir' ],
#      }
# }
#

define tivoli_client::inclexcl(
    $ensure = present,
    $xfiles = [],
    $xdirectories = [],
    $xfs = [],
    $ifiles = [])
{
    case $ensure {
        present: {
            file {
                "/etc/tivoli/inclexcl":
                    content => template("tivoli_client/inclexcl.erb"),
                    notify  => Service["dsmc"];
            }
        }
        absent: {
            # in case someone wants to ensure the file doesn't exist
            file { "/etc/tivoli/inclexcl": ensure => absent }
        }
        # doesn't do anything
        unmanaged: {
        }
        default: { crit "Invalid ensure value: $ensure" }
    }
}
