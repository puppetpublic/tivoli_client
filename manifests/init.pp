#
# Set up a Tivoli backup client.

class tivoli_client {
  include compatlibs,
      base::libstdc

  case $::operatingsystem {
    'redhat': {
      case $::lsbdistrelease {
        # rhel4 only required packages.
        # Not needed for rhel3 and currently unknown for rhel5
        4: {
          # install i386 ROM
          exec {
            'tivoli client required xorg-x11 libs':
              command => 'up2date --arch=i386 xorg-x11-libs',
              unless  =>
                "rpm -q --qf \"%{ARCH}\n\" xorg-x11-libs | grep -q i386";
            'tivoli client required xorg-x11-deprecated libs':
              command => 'up2date --arch=i386 xorg-x11-deprecated-libs',
              unless  =>
                "rpm -q --qf \"%{ARCH}\n\" xorg-x11-deprecated-libs | grep -q i386";
          }
        }
        default: {}
      }

      # setup some variables specific to newer tivoli on rhel5
      if ( $::architecture == 'x86_64' and $::lsbmajdistrelease >= 5 ) {
        $tivlib='/opt/tivoli/tsm/client/api/bin64'
      }
      else {
        $tivlib='/opt/tivoli/tsm/client/api/bin'
      }
      $tivbin='/opt/tivoli/tsm/client/ba/bin'
      $tivBA='TIVsm-BA'

      package {
        $tivBA:
          ensure  => present,
          require => $::lsbdistrelease ? {
            3       => [  Package['libstdc++'] ],
            default => [  Package['libstdc++'], Package['compat-libstdc++-296'], ],
          };
      }

      # soft link binaries and libs since the pkg install is inconsistent
      file {
        # binaries
        '/usr/bin/dsmadmc':
          ensure  => link,
          target  => "${tivbin}/dsmadmc",
          require => Package[$tivBA];
        '/usr/bin/dsmagent':
          ensure  => link,
          target  => "${tivbin}/dsmagent",
          require => Package[$tivBA];
        '/usr/bin/dsmc':
          ensure  => link,
          target  => "${tivbin}/dsmc",
          require => Package[$tivBA];
        '/usr/bin/dsmcad':
          ensure  => link,
          target  => "${tivbin}/dsmcad",
          require => Package[$tivBA];
        '/usr/bin/dsmj':
          ensure  => link,
          target  => "${tivbin}/dsmj",
          require => Package[$tivBA];
        '/usr/bin/dsmswitch':
          ensure  => link,
          target  => "${tivbin}/dsmswitch",
          require => Package[$tivBA];
        '/usr/bin/dsmtca':
          ensure  => link,
          target  => "${tivbin}/dsmtca",
          require => Package[$tivBA];
        # libs
        '/usr/lib/libApiDS.so':
          ensure  => link,
          target  => "${tivlib}/libApiDS.so",
          require => Package[$tivBA];
        '/usr/lib/libct_cu.so':
          ensure  => link,
          target  => "${tivlib}/libct_cu.so",
          require => Package[$tivBA];
        '/usr/lib/libdmapi.so':
          ensure  => link,
          target  => "${tivlib}/libdmapi.so",
          require => Package[$tivBA];
        '/usr/lib/libgpfs.so':
          ensure  => link,
          target  => "${tivlib}/libgpfs.so",
          require => Package[$tivBA];
        '/usr/lib/libha_gs_r.so':
          ensure  => link,
          target  => "${tivlib}tivlib/libha_gs_r.so",
          require => Package[$tivBA];
      }

      # Install the init script.
      file { '/etc/init.d/dsmc':
        source => 'puppet:///modules/tivoli_client/init.dsmc.RedHat',
        mode   => '0775',
        notify => Exec['add dsmc'],
      }
      exec { 'add dsmc':
        command => '/sbin/chkconfig --add dsmc',
        require => File['/etc/init.d/dsmc'],
        unless  => '/sbin/chkconfig --list dsmc',
      }
    }

    'debian', 'ubuntu': {
    $tivBA='tivsm-ba'
    if ( $::architecture == 'amd64' and $::lsbmajdistrelease >= 7 ) {
      $tivlib='/opt/tivoli/tsm/client/api/bin64'
    }
    else {
      $tivlib='/opt/tivoli/tsm/client/api/bin'
    }
      package {
        $tivBA:  ensure => present;
      }

      # Install the init script.
      file { '/etc/init.d/dsmc':
        source => 'puppet:///modules/tivoli_client/init.dsmc.Debian',
        mode   => '0755',
        notify => Exec['update-rc.d dsmc'],
      }
      exec { 'update-rc.d dsmc':
        command     => 'update-rc.d dsmc defaults',
        refreshonly => true,
      }
    }
  }

  file {
    '/opt/tivoli/tsm/client/ba/bin/dsm.sys':
      ensure  => link,
      target  => '/etc/tivoli/dsm.sys',
      require => [ Package[$tivBA] ];
    '/opt/tivoli/tsm/client/ba/bin/dsm.opt':
      ensure  => link,
      target  => '/etc/tivoli/dsm.opt',
      require => [ Package[$tivBA] ];
    '/opt/tivoli/tsm/client/ba/bin/inclexcl':
      ensure  => link,
      target  => '/etc/tivoli/inclexcl',
      require => [ Package[$tivBA] ];
    # This is required for RMAN backups.  Add dsm.sys under api.
    "${tivlib}/dsm.sys":
      ensure  => link,
      target  => '/etc/tivoli/dsm.sys',
      require => [ Package[$tivBA] ],
  }

  # Configure default inclexcl file.
  tivoli_client::inclexcl { $::fqdn_lc: ensure => present }

  # Ensure dsmc is running.
  service { 'dsmc':
    ensure    => running,
    require   => File['/etc/init.d/dsmc'],
    hasstatus => false,
    status    => 'pidof dsmc || test ! -f /etc/adsm/TSM.PWD',
  }

  # Make sure the /etc/tivoli directory is there.
  file { '/etc/tivoli': ensure => directory }

  # Call the defintion file.
  tivoli_client::config { $::fqdn_lc: nodename => $::hostname }
}
